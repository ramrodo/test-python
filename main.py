import pandas as pd

def hola(word):
    """
    >>> hola("rodo")
    'Hola mundo'
    """
    return 'Hola mundo'
    
def multiply(n1, n2):
    """
    >>> multiply(4, 5)
    20
    """
    return n1*n2

if __name__ == "__main__":
    hola("rodo")
    print(pd.__version__)
